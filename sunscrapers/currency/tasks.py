import logging

from django.conf import settings
from huey import crontab
from huey.contrib.djhuey import db_periodic_task

from .models import store_currency_rate
from .data_sources import ecb


log = logging.getLogger(__name__)

# TODO: get dynamically from xpath?
URLS = [
    'https://www.ecb.europa.eu/rss/fxref-usd.html',
]


@db_periodic_task(crontab(**settings.CRON_TASK_PERIOD))
def currency_rates_ecb():
    for url in URLS:
        try:
            currency_data = ecb.get_currency_data(url)
            store_currency_rate(currency_data)
        except Exception as e:
            log.exception(e)
            continue
