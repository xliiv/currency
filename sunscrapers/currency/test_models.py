from django.test import TestCase


from currency.models import store_currency_rate


class TestStoreCurrencyRate(TestCase):
    def test_currency_rate_is_stored_when_data_ok(self):
        currency_data = {
            'exchange_rate': '4.1844',
            'source_currency': 'EUR',
            'source_uri': 'http://www.ecb.europa.eu/stats/exchange/eurofxref/html/eurofxref-graph-pln.en.html?date=2018-04-12&rate=4.1844',
            'target_currency': 'PLN',
            'updated': '2018-04-12T14:15:00+01:00',
        }

        obj = store_currency_rate(currency_data)

        self.assertEqual(obj.exchange_rate, currency_data['exchange_rate'])
        self.assertEqual(obj.source_currency.symbol, currency_data['source_currency'])
        self.assertEqual(obj.target_currency.symbol, currency_data['target_currency'])
        self.assertEqual(obj.source_uri, currency_data['source_uri'])
        self.assertEqual(obj.updated, currency_data['updated'])
