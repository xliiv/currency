from rest_framework import serializers

from ..models import Currency, CurrencyRate


class CurrencySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Currency
        fields = ('symbol',)


class CurrencyRateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CurrencyRate
        fields = '__all__'
