from django.conf.urls import url, include
from rest_framework import routers

from . import views


router = routers.DefaultRouter()
router.register(r'currency', views.CurrencyViewSet)
router.register(r'currency-rate', views.CurrencyRateViewSet)
