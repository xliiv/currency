from django.db import models


class Currency(models.Model):
    symbol = models.CharField(max_length=8, unique=True)


class CurrencyRate(models.Model):
    target_currency = models.ForeignKey(Currency, on_delete=models.CASCADE, related_name="target_currency")  # fk
    source_currency = models.ForeignKey(Currency, on_delete=models.CASCADE, related_name="source_currency")  # fk
    exchange_rate = models.DecimalField(max_digits=12, decimal_places=4)  # "4.1915"
    updated = models.DateTimeField()
    source_uri = models.URLField(max_length=2000)

    created_timestamp = models.DateTimeField(auto_now_add=True)
    updated_timestamp = models.DateTimeField(auto_now=True)


def store_currency_rate(currency_data):
    target, _ = Currency.objects.get_or_create(symbol=currency_data['target_currency'])
    source, _ = Currency.objects.get_or_create(symbol=currency_data['source_currency'])
    currency_rate = CurrencyRate(
        target_currency=target,
        source_currency=source,
        exchange_rate=currency_data['exchange_rate'],
        updated=currency_data['updated'],
        source_uri=currency_data['source_uri'],
    )
    currency_rate.save()
    return currency_rate
