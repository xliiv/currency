import os
import unittest

from .ecb import get_currency_data


class TestECB(unittest.TestCase):

    def setUp(self):
        self.data_url = os.path.join(
            os.path.dirname(__file__), 'test_data_ecb_pln.html'
        )

    def test_data_is_extracted_correctly_when_data_ok(self):
        found = get_currency_data(self.data_url)
        self.assertEqual(
            found,
            {
                'exchange_rate': '4.1844',
                'source_currency': 'EUR',
                'source_uri': 'http://www.ecb.europa.eu/stats/exchange/eurofxref/html/eurofxref-graph-pln.en.html?date=2018-04-12&rate=4.1844',
                'target_currency': 'PLN',
                'updated': '2018-04-12T14:15:00+01:00',
            }
        )


if __name__ == '__main__':
    unittest.main()
