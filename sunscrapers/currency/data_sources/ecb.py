import feedparser


def feed2currency_rate_data(feed):
    data = {}
    last_entry = feed['entries'][0]
    rate, source_currency = last_entry['cb_exchangerate'].split('\n')
    data['exchange_rate'] = rate
    data['source_currency'] = source_currency
    data['target_currency'] = last_entry['cb_targetcurrency']
    data['updated'] = last_entry['updated']
    data['source_uri'] = last_entry['id']
    return data


# TODO: asyncio?
def get_currency_data(url):
    feed = feedparser.parse(url)
    data = feed2currency_rate_data(feed)
    return data
