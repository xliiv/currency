from django.contrib import admin

from .models import Currency, CurrencyRate

admin.site.register(CurrencyRate)
admin.site.register(Currency)
