# Currency

## Installation

Prerequisites:
* redis or docker
* python3

```bash
virtualenv -p python3 _venv
. _venv/bin/activate
pip install -r requirements.txt
cd ./sunscrapers
python manage.py migrate
python manage.py createsuperuser

# run redis by docker (if docker not available provide redis server for your own)
docker run --rm --name currency redis
# run queue consumer
./manage.py run_huey
# or with specified redis server
# REDIS_URL="redis://172.17.0.2:6379/?db=1" ./manage.py run_huey

# run development server
python manage.py runserver
# open in your browser: http://127.0.0.1:8000/
```

## Arch

This application is built using:

* [Django](https://www.djangoproject.com/) (Web Framework)
* [Django Rest Framework](http://www.django-rest-framework.org/) (REST API)
* [Huey](http://huey.readthedocs.io/en/latest/index.html) (Task queue)


Workflow:

Currency data is collected in background task.
Task is run on each every minute.
This is customizable by `settings.CRON_TASK_PERIOD`.



## TODO:

* rest-api: token authentication
* docstrings
* limit REST to GET
* do not store history
* docker-compose?
* currency sources stored in database or dynamically scrapped by xpath
* CI
